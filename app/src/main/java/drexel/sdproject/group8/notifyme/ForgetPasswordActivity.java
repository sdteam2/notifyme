package drexel.sdproject.group8.notifyme;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ForgetPasswordActivity extends AppCompatActivity implements Validator.ValidationListener {


    @Email
    @NotEmpty
    EditText inputEmail;

    Button submit, cancel;
    private ProgressDialog pDialog;
    private boolean send = true, datavalid = false, trytosend = false;
    private static final String TAG_SUCCESS = "success";
    JSONParser jsonParser = new JSONParser();
    String email;
    Validator validator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        validator = new Validator(this);
        validator.setValidationListener(this);
        inputEmail=(EditText)findViewById(R.id.input_email);
        submit = (Button) findViewById(R.id.btn_submit);
        cancel = (Button) findViewById(R.id.button3);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email=inputEmail.getText().toString();
                validator.validate();
                if (datavalid) {
                    new SendInquiry().execute();
                    trytosend = true;
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i2);
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        datavalid = true;
    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }


    class SendInquiry extends AsyncTask<String, Integer, Integer> {
        String result = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ForgetPasswordActivity.this);
            pDialog.setMessage("Please Wait.....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected Integer doInBackground(String... args) {

            Integer suc = new Integer(1);
            Integer fail = new Integer(0);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", email));
            JSONObject json = jsonParser.makeHttpRequest(Constants.URL_FORGET, "GET", params);
            try {
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    return suc;
                }
                else
                {
                    return fail;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return fail;
        }

        protected void onPostExecute(Integer result) {
            if (result.equals(1)) {
                new AlertDialog.Builder(ForgetPasswordActivity.this)
                        .setTitle("Email sent Successfully")
                        .setMessage("An email was sent to your email address with your password. Please check your email.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                                startActivity(i);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_email)
                        .show();
            }
        }
    }
}
