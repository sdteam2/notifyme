package drexel.sdproject.group8.notifyme;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty
    EditText inputFName;
    @NotEmpty
    EditText inputLName;
    @Email
    @NotEmpty
    EditText inputEmail;
    @Password
    EditText inputPassword;
    @ConfirmPassword
    EditText confirmPassword;

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    Button btn_register, btn_cancle;
    Validator validator;
    private ProgressDialog pDialog;
    private boolean send = true, datavalid = false, trytosend = false;
    JSONParser jsonParser = new JSONParser();
    String fname, lname, email, password;
    private static final String TAG_SUCCESS = "success";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        validator = new Validator(this);
        validator.setValidationListener(this);
        inputFName = (EditText) findViewById(R.id.input_fname);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputLName = (EditText) findViewById(R.id.input_lname);
        inputPassword = (EditText) findViewById(R.id.input_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        btn_register = (Button) findViewById(R.id.btn_register);
        radioGroup = (RadioGroup) findViewById(R.id.radioChoice1);
        btn_cancle = (Button) findViewById(R.id.btn_cancle);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
                fname = inputFName.getText().toString();
                email = inputEmail.getText().toString();
                lname = inputLName.getText().toString();
                password = inputPassword.getText().toString();
                radioButton = (RadioButton) findViewById(radioGroup.getCheckedRadioButtonId());
                if((radioButton.getText().equals("Student")) && (datavalid=true)) {
                    new SendInquiry().execute();
                }
                else if((radioButton.getText().equals("Faculty")) && (datavalid=true)){
                   new FacultyInquiry().execute();
                }
                trytosend = true;
            }
        });

        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i1 = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i1);
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        datavalid = true;
    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                datavalid=false;
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    class SendInquiry extends AsyncTask<String, Integer,Integer> {
        String result = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setMessage("Please Wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected Integer doInBackground(String... args) {
            Integer suc = new Integer(1);
            Integer fail = new Integer(0);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("fname", fname));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("lname", lname));
            params.add(new BasicNameValuePair("password", password));
            JSONObject json = jsonParser.makeHttpRequest(Constants.URL_REGISTER_STUDENT, "GET", params);
            try {
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    return suc;
                }
                else
                {
                    return fail;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return fail;
        }

        protected void onPostExecute(Integer result) {
            if (result.equals(1)) {
                new AlertDialog.Builder(RegisterActivity.this)
                        .setTitle("Registration Successful")
                        .setMessage("A verification email has been sent to your email id. Please verify the email address before you try to login.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                                startActivity(i);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_email)
                        .show();
            }
        }
    }

    class FacultyInquiry extends AsyncTask<String, Integer,Integer> {
        String result = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(RegisterActivity.this);
            pDialog.setMessage("Please Wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected Integer doInBackground(String... args) {
            Integer suc = new Integer(1);
            Integer fail = new Integer(0);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("fname", fname));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("lname", lname));
            params.add(new BasicNameValuePair("password", password));
            JSONObject json = jsonParser.makeHttpRequest(Constants.URL_REGISTER_FACULTY, "GET", params);
            try {
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    return suc;
                }
                else
                {
                    return fail;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return fail;
        }

        protected void onPostExecute(Integer result) {
            if (result.equals(1)) {
                new AlertDialog.Builder(RegisterActivity.this)
                        .setTitle("Registration Successful")
                        .setMessage("A verification email has been sent to your email id. Please verify the email address before you try to login.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                                startActivity(i);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_email)
                        .show();
            }
        }
    }
}
