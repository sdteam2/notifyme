package drexel.sdproject.group8.notifyme;

/**
 * Created by Taral's on 21-Nov-16.
 */

public class Constants {
    public static final String URL_DOMAIN = "http://144.118.242.80:7777/";
    public static final String URL_PATH = "notifyme/";
    public final static String URL_LOGIN = URL_DOMAIN + URL_PATH + "login_script.php";
    public final static String URL_FORGET = URL_DOMAIN+URL_PATH+"forget_password.php";
    public final static String URL_REGISTER_STUDENT = URL_DOMAIN+URL_PATH+"register_student.php";
    public final static String URL_REGISTER_FACULTY = URL_DOMAIN+URL_PATH+"register_faculty.php";
    public static final String TAG_FIRSTNAME = "fname";
    public static final String TAG_PASSWORD = "password";
    public static final String TAG_LASTNAME = "lname";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_EMAIL = "email";

}
