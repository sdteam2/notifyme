package drexel.sdproject.group8.notifyme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {


    @Email
    @NotEmpty
    EditText inputEmail;
    @Password
    EditText inputPassword;

    Validator validator;
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    String email, pass, fname, lname,userType;
    private boolean send = true, datavalid = false, trytosend = false;
    TextView forget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        validator = new Validator(this);
        validator.setValidationListener(this);
        forget = (TextView) findViewById(R.id.textView);
        inputEmail = (EditText) findViewById(R.id.editText);
        inputPassword = (EditText) findViewById(R.id.editText2);
    }

    public void signIn(View v) {
        validator.validate();
        email = inputEmail.getText().toString();
        pass = inputPassword.getText().toString();
        new SendInquiry().execute();
        trytosend = true;
    }

    public void register(View v) {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    public void forgotPassword(View v) {

        Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onValidationSucceeded() {
        datavalid = true;
    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    class SendInquiry extends AsyncTask<String, Integer, Integer> {
        String result = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Signing In.....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected Integer doInBackground(String... args) {
            Integer stu = new Integer(1);
            Integer fac = new Integer(2);
            Integer fail = new Integer(0);
            InputStream is = null;
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("password", pass));
            JSONObject json = jsonParser.makeHttpRequest(Constants.URL_LOGIN, "GET", params);
            try {
                int sucess = json.getInt("success");
                Log.d("login success", "" + sucess);
                if (sucess == 1) {
                    parseResult(json);
                    if(userType.equals("student"))
                    {
                        return stu;
                    }
                    else if(userType.equals("faculty")){
                        return fac;
                    }
                } else {
                    return fail;
                }
            } catch (Exception e) {
            }
            return fail;
        }


        protected void onPostExecute(Integer result) {
            //pDialog.dismiss();
            pDialog.dismiss();
            if (result.equals(1)) {
                Intent intent = new Intent(LoginActivity.this, StudentMainActivity.class);
                startActivity(intent);
                finish();
            } else if (result.equals(2)){
                Intent intent = new Intent(LoginActivity.this, FacultyMainActivity.class);
                startActivity(intent);
                finish();
            }else{
                Toast.makeText(getApplicationContext(), "Invalid User Name or Password", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void parseResult(JSONObject result) {

        try {


            Log.d("inpost", "in parse result");
            //JSONObject response = new JSONObject(result);
            JSONArray posts = result.optJSONArray("detail");
            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                fname = post.getString("fname");
                Log.d("fName", fname);
                lname = post.getString("lname");
                Log.d("lname", lname);
                userType = post.getString("userType");
                Log.d("userType",userType);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
