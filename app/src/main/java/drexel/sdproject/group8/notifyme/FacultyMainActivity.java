package drexel.sdproject.group8.notifyme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FacultyMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_main);
    }
}
